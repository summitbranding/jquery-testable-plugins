# Testable jQuery Plugin Patterns

## Factory approach
### Overview
All methods and properties related to the plugin are stored in one or more object literals. When the jQuery selector extension is used (<code>$.fn.plugnName = function (options) {}</code>) it will call the plugin's <code>init</code> function.

A second plugin will be created right off the jQuery object (<code>$.pluginName = {}</code>) inside of which a test method will be created which will programatically run one of the methods stored in the plugin object and return its return value, as well as any stateful objects (properties) of the plugin.

### Links
* <a href="js/v1/plugin-pattern-1.js">Plugin pattern 1</a>
* <a href="js.test/v1/plugin-pattern-1.spec.js">Plugin pattern 1 test</a>

## Global to the plugin, function approach
### Overview
All methods and properties will be stored right inside the Immediately-Invoked FunctionExpression (<a href="http://benalman.com/news/2010/11/immediately-invoked-function-expression/">IIFE</a>) - <code>;(function ($) {}(jQuery));</code>, along with the assignment of plugin and it's test plugin.

The test plugin will have 1 function for each function inside IIFE, excluding the plugin creations.

### Links
* <a href="js/v1/plugin-pattern-2.js">Plugin pattern 2</a>
* <a href="js.test/v1/plugin-pattern-2.spec.js">Plugin pattern 2 test</a>