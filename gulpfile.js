var
	config,
	gulp,
	karma;

gulp = require('gulp');
karma = require('karma').server;
config = require('./gulp.config.js')();

gulp.task('tdd', function () {
	karma.start({
		'configFile': __dirname + '/karma.config.js'
	});
});