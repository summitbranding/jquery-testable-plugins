describe('pattern 1 tests', function () {
	
	var
		options,
		trigger;
	
	beforeEach(function () {
		// prepare variables
		options = {
			'first': '1st'
		};
		trigger = '<button></button>';
		
	});
	
	afterEach(function () {
		// clean out variables
		options = null;
		trigger = null;
		
	});
	
	it('should initialize settings', function () {
		var results = $.plugin1.testMethod('init', [options, trigger]);
		
		expect(results.settings).not.toBeNull();
		expect(results.settings.first).toEqual('1st');
	});
	
	it('should initialize $elements', function () {
		var results = $.plugin1.testMethod('init', [options, trigger]);
		expect(results.$elements).toEqual({
			'trigger': $(trigger)
		});
	});
	
	it('function addNumbers, should add numbers and only numbers', function () {
		var numbers = [1, 2, 3, 4, 5],
			mixed = [1, 'two', 3, false, 5],
			singleNumber = 5,
			singleTrue = true;
		
		expect($.plugin1.testMethod('addNumbers', [numbers]).returnValue).toEqual(15);
		expect($.plugin1.testMethod('addNumbers', [mixed]).returnValue).toEqual(9);
		expect($.plugin1.testMethod('addNumbers', [singleNumber]).returnValue).toEqual(5);
		expect($.plugin1.testMethod('addNumbers', [singleTrue]).returnValue).toEqual(0);
	});
	
});