describe('pattern 2 tests', function () {
	var
		options,
		trigger;
	
	beforeEach(function () {
		
		options = {
			'second': '2nd',
			'type': 'link'
		};
		trigger = '<a href="http://www.ancestry.com">Link Text</a>';
	});
	
	afterEach(function () {
		options = null;
		trigger = null;
	});
	
	it('should initialize settings', function () {
		var results = $.plugin2Test.init(options);
		expect(results.settings).toEqual({
			'type': 'link',
			'autoinstantiate': true,
			'second': '2nd'
		});
	});
	
	it('function addNumbers, should add numbers, and only numbers', function () {
		var numbers = [1, 2, 3, 4, 5],
			mixed = [1, 'two', 3, false, 5],
			singleNumber = 5,
			singleTrue = true;
		
		expect($.plugin2Test.addNumbers(numbers)).toEqual(15);
		expect($.plugin2Test.addNumbers(mixed)).toEqual(9);
		expect($.plugin2Test.addNumbers(singleNumber)).toEqual(5);
		expect($.plugin2Test.addNumbers(singleTrue)).toEqual(0);
	});
	
});