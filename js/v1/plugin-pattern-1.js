;(function ($) {
	var plugin1,
		$elements,
		defaults,
		settings;
	
	defaults = {
		'first': ''
	};
	
	plugin1 = {
		'init': function (options, trigger) {
			// init functionality
			settings = $.extend({}, defaults, options);
			$elements = {
				'trigger': $(trigger)
			};
		},
		'printMessage': function(msg) {
			console.log(msg);
		},
		'addNumbers': function(numbers) {
			var sum = 0;
			if (Array.isArray(numbers)) {
				numbers.forEach(function (currentValue) {
					if ($.isNumeric(currentValue)) {
						sum += currentValue;
					}
				});
			} else {
				if ($.isNumeric(numbers)) {
					sum = numbers;
				}
			}
			return sum;
		}
	};
	
	$.fn.plugin1 = function(options) {
		return this.each(function (_index, trigger) {
			plugin1.init(options, trigger);
		});
	};
	
	$.plugin1 = {
		'testMethod': function(methodName, parameterArray, optionalThis) {
			return {
				'returnValue': plugin1[methodName].apply(optionalThis || {}, parameterArray),
				'settings': $.extend({}, settings),
				'$elements': $elements
			};
		}
	};
}(jQuery));