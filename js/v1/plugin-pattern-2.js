;(function ($) {
	var
		defaults,
		settings;
	
	defaults = {
		'autoinstantiate': true,
		'type': 'button'
	};
	
	function addNumbers(numbers) {
		var sum = 0;
		if (Array.isArray(numbers)) {
			numbers.forEach(function (currentValue) {
				if ($.isNumeric(currentValue)) {
					sum += currentValue;
				}
			});
		} else {
			if ($.isNumeric(numbers)) {
				sum = numbers;
			}
		}
		return sum;
	}
	
	function init(options) {
		settings = $.extend({}, defaults, options);
	}
	
	$.fn.plugin2 = function (options) {
		return this.each(function () {
			init(options);
		});
	};
	
	$.plugin2Test = {
		'init': function (options) {
			return {
				'returnValue': init(options),
				'settings': settings
			};
		},
		'addNumbers': function(numbers) {
			return addNumbers(numbers);
		}
	};
}(jQuery));