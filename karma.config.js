module.exports = function(config) {
	config.set({
		'basePath': '',
		
		'frameworks': [
			'jasmine'
		],
	
		'files': [
			{
				// Library files - just jQuery here
				// These will be included on tests, but not watched for changes
				'pattern': './lib/jquery/**/*.js',
				'watched': false,
				'included': true,
				'served': true
			},
			
			// Our working files
			'./js/**/*.js',
			
			// Our test files
			'./js.test/**/*.spec.js'
		],
		
		'port': 9876,
		
		'colors': true,
		
		'autowatch': true,
		
		'browsers': [
			'PhantomJS'
		],
		
		'reporters': [
			'spec'
		],
		
		'singleRun': false
	});
};